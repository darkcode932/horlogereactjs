import React, { Component } from 'react';
import './App.css'


const jours = ["DIMANCHE", "LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"];
const mois = ["JANVIER", "FEVRIER", "mars", "avril", "mai", "juin", "JUILLET", "Aout", "septembre", "octobre", "novembre", "Decembre"];

class App extends Component {


    state = {
        currentDate: new Date(),
        editing: false,
        edited: false
    }


    componentDidMount() {
        this.timeID = setInterval(() => this.tick(), 1000);
    }


    componentWilUnmount() {
        clearInterval(this.timeID);
    }


    handleChange = (e) => {


        if (!e.target.value) return false;


        const newDate = this.state.currentDate;


        if (e.target.name === "mytime") {
            const hour_minute = e.target.value.split(":");
            newDate.setHours(hour_minute[0], hour_minute[1]);
        } else {

            const day_month_year = e.target.value.split("-");
            newDate.setFullYear(parseInt(day_month_year[0]))
            newDate.setMonth(parseInt(day_month_year[1]) - 1)
            newDate.setDate(parseInt(day_month_year[2]))
        }


        this.setState({ currentDate: newDate, edited: true })

    }


    toggleMode = (e) => {
        this.setState({ editing: e.target.checked });
    }


    resetChanges = () => {
        this.setState({ currentDate: new Date(), edited: false });
    }


    tick() {
        if (!this.state.edited) {
            this.setState({
                currentDate: new Date()
            });
        }
    }


    ajouteZero = (n) => n > 9 ? n : "0" + n;


    render() {


        const { currentDate } = this.state;


        const day = currentDate.getDay();
        const date = this.ajouteZero(currentDate.getDate());
        const month = currentDate.getMonth();
        const year = currentDate.getFullYear();

        const hour = this.ajouteZero(currentDate.getHours());
        const minutes = this.ajouteZero(currentDate.getMinutes());


        let periode = "MATIN";
        if (hour > 12 && hour < 16) {
            periode = "APRES-MIDI";
        }
        else if (hour >= 16) {
            periode = "SOIR";
        }
        else if(hour === 12){
            periode = "MIDI";
        }


        return (
            <div className="container">

                <div>

                    <form className="form">

                        <div>
                            <input type="checkbox" id="editing" onChange={this.toggleMode} />
                            <label htmlFor="editing" className={this.state.editing ? 'active' : ""}>

                                {!this.state.editing ? "Cliquez ici pour modifier" : "Masquer la modification"}
                            </label>
                        </div>


                        {this.state.editing && (<div class="fields">
                            <input type="date" name="mydate" defaultValue={`${year}-${this.ajouteZero(month)}-${date}`} onChange={this.handleChange} />
                            <input type="time" name="mytime" defaultValue={`${hour}:${minutes}`} onChange={this.handleChange} />
                        </div>)}

                    </form>

                    <div className="firstblock">

                        <div className="secondblock">
                            <span className="day">{jours[day]}</span><br />
                            <span className="date">{periode}</span>
                            <div className="hour">{hour}:{minutes}</div>
                            <div className="date">{date} {mois[month].toUpperCase()} {year}</div>
                            <button className="reset" onClick={this.resetChanges}>Annuler les modifications</button>
                        </div>


                    </div>

                </div>
            </div>

        );
    }
}

export default App;